def mean(first, *numbers):
    summa = sum(numbers) + first
    return summa / (len(numbers) + 1)


print("Mean (15, 4): {}".format(mean(15, 4)))
print("Mean (2, 67, 45, 16, 22, 7, 15, 90): {}".format(mean(2, 67, 45, 16, 22, 7, 15, 90)))

